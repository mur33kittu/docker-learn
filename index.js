const express = require('express');

const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser());

app.get('/', (req, res, next) => {
  res.send('Hello World');
});
app.listen(3000, function (req, res, error) {
  console.log('Server listening in port 3000');
});
